﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NET03_ASSIGN02.Models;

namespace NET03_ASSIGN02.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BranchesController : ControllerBase
    {
        private readonly BranchContext _context;

        public BranchesController(BranchContext context)
        {
            _context = context;
        }

        // GET: api/Branches
        [HttpGet]
        public async Task<ActionResult<IEnumerable<BranchDTO>>> GetBranches()
        {
            if (_context.Branches == null)
            {
                return NotFound();
            }
            return await _context.Branches.Select(b => BranchToDTO(b)).ToListAsync();
        }

        // GET: api/Branches/5
        [HttpGet("{id}")]
        public async Task<ActionResult<BranchDTO>> GetBranch(int id)
        {
          if (_context.Branches == null)
          {
              return NotFound();
          }
            var branch = await _context.Branches.FindAsync(id);

            if (branch == null)
            {
                return NotFound();
            }

            return BranchToDTO(branch);
        }

        // PUT: api/Branches/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBranch(int id, BranchDTO branchDTO)
        {
            if (id != branchDTO.Id)
            {
                return BadRequest();
            }

            //_context.Entry(branchDTO).State = EntityState.Modified;

            var branch = await _context.Branches.FindAsync(id);
            if (branch == null)
            {
                return NotFound();
            }

            branch.Name = branchDTO.Name;
            branch.Address = branchDTO.Address;
            branch.City = branchDTO.City;
            branch.State = branchDTO.State;
            branch.ZipCode = branchDTO.ZipCode;


            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BranchExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Branches
        [HttpPost]
        public async Task<ActionResult<BranchDTO>> PostBranch(BranchDTO branchDTO)
        {
          if (_context.Branches == null)
          {
              return Problem("Entity set 'BranchContext.Branches'  is null.");
          }
            var branch = new Branch
            {
                Name = branchDTO.Name,
                Address = branchDTO.Address,
                City = branchDTO.City,
                State = branchDTO.State,
                ZipCode = branchDTO.ZipCode
            };
            _context.Branches.Add(branch);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetBranch), new { id = branch.Id }, BranchToDTO(branch));
        }

        // DELETE: api/Branches/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBranch(int id)
        {
            if (_context.Branches == null)
            {
                return NotFound();
            }
            var branch = await _context.Branches.FindAsync(id);
            if (branch == null)
            {
                return NotFound();
            }

            _context.Branches.Remove(branch);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool BranchExists(int id)
        {
            return _context.Branches.Any(e => e.Id == id);//.GetValueOrDefault()
        }
        private static BranchDTO BranchToDTO(Branch br) => new BranchDTO
        {
           Id = br.Id,
           Name = br.Name,
           Address = br.Address,
           City = br.City,
           State = br.State,
           ZipCode = br.ZipCode
        };
    }
}
