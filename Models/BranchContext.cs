﻿using Microsoft.EntityFrameworkCore;

namespace NET03_ASSIGN02.Models
{
    public class BranchContext:DbContext
    {
        public BranchContext(DbContextOptions<BranchContext> options)
            : base(options) {
        
        }
        public DbSet<Branch> Branches { get; set; } = null!;
    }
}
