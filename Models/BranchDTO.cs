﻿using Newtonsoft.Json;

namespace NET03_ASSIGN02.Models
{
    public class BranchDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Address { get; set; }
        public string? City { get; set; }
        public string? State { get; set; }
        public string? ZipCode { get; set; }
    }
}
